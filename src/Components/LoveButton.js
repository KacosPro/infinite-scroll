import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as faHeartRegular } from '@fortawesome/free-regular-svg-icons';
import { faHeart as faHeartSolid } from '@fortawesome/free-solid-svg-icons';

class LoveButton extends Component {
  state = {hearted: false}

  toggleHearted() {
    this.setState((prevState) => {
      return { hearted: !prevState.hearted }
    });
  }

  getIcon() {
    const { hearted } = this.state;
    const size = '2x';
    const icon = hearted ? faHeartSolid : faHeartRegular;
    const color = hearted ? 'red' : 'black';

    return <FontAwesomeIcon icon={icon} color={color} size={size} />
  }

  render() {
    return (
      <button onClick={() => this.toggleHearted()} type="button" className="btn btn-link p-0">
        {this.getIcon()}
      </button>
    );
  }
}

export default LoveButton;
