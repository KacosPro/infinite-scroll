import React from 'react';

import LoveButton from './LoveButton';

const MyCard = (props) => {
  const { imageUrl, title } = props.recipe;

  return (
  <div className="row justify-content-center">
    <div className="col col-md-10 col-lg-6 mt-4 mb-4">
      <div className="card shadow">
        <img className="card-img-top" src={imageUrl} alt={title} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <div className="d-flex justify-content-end">
            <LoveButton />
          </div>
        </div>
      </div>
    </div>
  </div>
  );
}

export default MyCard;
