import React, { Component } from 'react';
import { MyCard } from './Components';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      recipes: [
        {
          id: 1,
          title: 'Puppy  1',
          imageUrl: 'https://loremflickr.com/320/240/puppy?random=1',
        },
        {
          id: 2,
          title: 'Puppy  2',
          imageUrl: 'https://loremflickr.com/320/240/puppy?random=2',
        },
        {
          id: 3,
          title: 'Puppy 3',
          imageUrl: 'https://loremflickr.com/320/240/puppy?random=3',
        }
      ]
    }
  }

  componentDidMount() {
    setInterval(() => this.checkScrollPosition(), 500);
  }

  checkScrollPosition() {
    const offset = 250;
    const scrollBottom = window.scrollY + window.innerHeight;
    const scrollHeight = document.documentElement.scrollHeight;

    if (scrollBottom > (scrollHeight - offset)) {
      this.fetchRecipes();
    }
  }

  fetchRecipes() {
    this.setState(prevState => {
      const { recipes } = prevState;
      const nextId = recipes.length + 1;

      recipes.push({
        id: nextId,
        title: `Puppy ${nextId}`,
        imageUrl: `https://loremflickr.com/320/240/puppy?random=${nextId}`,
      });

      return recipes
    });
  }

  render() {
    return (
      <div className="container">
        {this.state.recipes.map(recipe => <MyCard key={`recipe-${recipe.id}`} recipe={recipe} />)}
      </div>
    );
  }
}

export default App;
